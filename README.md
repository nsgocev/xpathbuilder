# SPEAK XPath Builder #

With the developer center removed from Sitecore 7.5+ the XPath Builder is now gone. This module is a simple SPEAK replacement of the XPath Builder. 

## How does it work ? ##

After the module is installed it can be accessed from the Launch Pad.


## Supported Versions##
Sitecore CMS 7.5

## Version History ##

### 0.1 Initial Release ###

Provides the basic XPath Builder Functionality

### 0.2 Update 1 ###

New Features:

  * Search Result Items now have icons

  * Query Execution Time is displayed

  * Total Results Count is displayed