﻿using Sitecore.Shell.Framework.Commands;
using Sitecore.Web.UI.Sheer;

namespace XPathBuilder.Web.Commands
{
    public class XPathBuilder : Command
    {
        public override void Execute(CommandContext context)
        {
            SheerResponse.ShowModalDialog("/sitecore/client/Applications/XPathBuilder/XPathBuilder?mode=framed", "1250px", "800px");
        }
    }
}