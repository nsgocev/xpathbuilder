﻿using Sitecore.Diagnostics;
using Sitecore.Mvc;
using Sitecore.Mvc.Presentation;
using System.Web;

namespace XPathBuilder.Web.Domain
{
    public static class ControlExtensions
    {
        public static HtmlString XPathBuilderQueryDataSource(this Controls controls, Rendering rendering)
        {
            Assert.ArgumentNotNull(controls, "controls");
            Assert.ArgumentNotNull(rendering, "rendering");
            return new HtmlString(new XPathBuilderQueryDataSource(controls.GetParametersResolver(rendering)).Render());
        }

        public static HtmlString XPathBuilderQueryDataSource(this Controls controls, string controlId, object parameters = null)
        {
            Assert.ArgumentNotNull(controls, "controls");
            Assert.ArgumentNotNull(controlId, "controlId");
            XPathBuilderQueryDataSource xPathBuilderQueryDataSource = new XPathBuilderQueryDataSource(controls.GetParametersResolver(parameters));
            xPathBuilderQueryDataSource.ControlId = controlId;
            return new HtmlString(xPathBuilderQueryDataSource.Render());
        }
    }
}