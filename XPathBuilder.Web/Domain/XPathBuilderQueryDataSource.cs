﻿using Sitecore;
using Sitecore.Diagnostics;
using Sitecore.Mvc.Presentation;
using Sitecore.Web.UI.Controls.Data;
using System;
using System.Web.UI;

namespace XPathBuilder.Web.Domain
{
    public class XPathBuilderQueryDataSource : ItemDataSourceBase, IPageble
    {

        public int PageIndex { get; set; }

        public string PageIndexBinding { get; set; }

        public int PageSize { get; set; }

        public string PageSizeBinding { get; set; }

        public string Query { get; set; }

        public string DatabaseName { get; set; }

        public string LanguageName { get; set; }

        protected string QueryBinding { get; set; }

        public XPathBuilderQueryDataSource()
        {
            Requires.Script("/sitecore/shell/client/Applications/XPathBuilder/Assets/XPathBuilderQueryDataSource.js");
            Query = string.Empty;
        }

        public XPathBuilderQueryDataSource(RenderingParametersResolver parametersResolver)
            : base(parametersResolver)
        {
            Assert.ArgumentNotNull(parametersResolver, "parametersResolver");
            Requires.Script("/sitecore/shell/client/Applications/XPathBuilder/Assets/XPathBuilderQueryDataSource.js");
            Query = parametersResolver.GetString("Query", "query");
            DatabaseName = parametersResolver.GetString("Database", "database");
            LanguageName = parametersResolver.GetString("Language", "language");
            ResolvePagingParameters(parametersResolver, this);
            QueryBinding = parametersResolver.GetString("QueryBinding");
        }

        protected override void PreRender()
        {
            base.PreRender();

            if (PageSize > 0)
            {
                SetAttribute("data-sc-pagesize", PageSize);
            }

            if (PageIndex > 0)
            {
                SetAttribute("data-sc-pageindex", PageIndex);
            }

            if (!string.IsNullOrEmpty(Query))
            {
                SetAttribute("data-sc-query", Query);
            }

            if (!string.IsNullOrEmpty(PageSizeBinding))
            {
                AddBinding("pageSize", PageSizeBinding);
            }

            if (!string.IsNullOrEmpty(PageIndexBinding))
            {
                AddBinding("pageIndex", PageIndexBinding);
            }

            if (!string.IsNullOrEmpty(QueryBinding))
            {
                AddBinding("query", QueryBinding);
            }

            if (!string.IsNullOrEmpty(LanguageName))
            {
                string languageName = LanguageName;

                if (string.Compare(languageName, "$context_language", StringComparison.InvariantCultureIgnoreCase) == 0)
                {
                    languageName = Context.Language.Name;
                }

                SetAttribute("data-sc-language", languageName);
            }

            if (!string.IsNullOrEmpty(DatabaseName))
            {
                string databaseName = DatabaseName;

                if (string.Compare(databaseName, "$context_database", StringComparison.InvariantCultureIgnoreCase) == 0)
                {
                    databaseName = Context.Database.Name;
                }
                else if (string.Compare(databaseName, "$context_contentdatabase", StringComparison.InvariantCultureIgnoreCase) ==
                         0)
                {
                    databaseName = Context.ContentDatabase.Name;
                }

                SetAttribute("data-sc-database", databaseName);
            }

            SetAttribute(HtmlTextWriterAttribute.Type, "text/x-sitecore-xpathbuilderquerydatasource");
        }

        protected override void Render(HtmlTextWriter output)
        {
            AddAttributes(output);
            output.RenderBeginTag(HtmlTextWriterTag.Script);
            output.RenderEndTag();
        }
    }
}
