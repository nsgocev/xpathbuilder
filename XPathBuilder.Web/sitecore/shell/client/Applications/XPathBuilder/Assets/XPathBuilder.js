define(["sitecore"], function(Sitecore) {
    var app = Sitecore.Definitions.App.extend({
        initialized: function() {

            var modeParameter = this.getParameterByName("mode");

            if (modeParameter != "" && modeParameter == "framed") {
                $(".sc-globalHeader").hide();
                $(".sc-applicationHeader-breadCrumb").hide();
            }

            this.StartItemTextBox.viewModel.$el.click($.proxy(function() {
                this.StartItemSelectorDialog.show();
            }, this));


            this.ResultXPathBuilderDataSource.on("change:isBusy", function() {
                this.rebindButtons();
                this.rebindIcons();
            }, this);


        },
        executeButtonClick: function() {

            var oldQuery = this.ResultXPathBuilderDataSource.get("query");
            var queryTextBoxValue = this.QueryTextBox.viewModel.$el.val();
            if (queryTextBoxValue === "") {
                //dirty hack to show null results
                this.ResultXPathBuilderDataSource.set("query", "/");
                alert('Empty Query Not Allowed');
            } else {
                if (queryTextBoxValue.indexOf('.') == 0
                    || queryTextBoxValue.indexOf('*') == 0) {

                    this.ResultXPathBuilderDataSource.set("query", this.StartItemTextBox.viewModel.$el.val() + "/" + queryTextBoxValue);


                } else {
                    this.ResultXPathBuilderDataSource.set("query", queryTextBoxValue);
                }
            }

            if (oldQuery == this.ResultXPathBuilderDataSource.get("query")) {
                this.ResultXPathBuilderDataSource.refresh();
            }
        },

        rebindButtons: function() {
            $("tr").click(function() {
                var itemID = $(this).children('td').eq(1)[0].innerText;
                if (itemID !== undefined) {
                    var win = window.open('/sitecore/shell/Applications/Content%20Editor.aspx?fo=' + itemID, '_blank');
                    if (win) {
                        win.focus();
                    } else {
                        alert('Please allow popups for this site');
                    }
                }
            });
        },

        rebindIcons: function() {
            var defaultIconPath = "~/icon/";
            $("img").each(function() {

                if (this.src.indexOf(defaultIconPath, this.src.length - defaultIconPath.length) !== -1) {
                    $(this).hide();
                }
            });
        },
        selectItem: function() {

            this.StartItemTextBox.viewModel.$el.val(this.ItemSelector.get("selectedItemPath"));
            this.StartItemSelectorDialog.hide();
        },
        getParameterByName: function(name) {
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        }
    });
    return app;
});